#!/bin/bash

# This script, install the DOS version of Gods (1991), and creates a desktop launch icon.
#
# You will need a zip of the game files

TITLE="Gods"
LINUXCONFIG="dosbox.conf"
LINUXEXECUTABLE="dosbox"
LINUXEXECUTABLEFOLDER=""
LINUXLAUNCHER="Launcher.sh"
LINUXICON="${TITLE}.png"
ICONURL="http://1.bp.blogspot.com/-Ybuf1CgzfxY/T3tYuH0OaTI/AAAAAAAAOsQ/gZikO0_Z2ls/s1600/Gods_%28Amiga%29_01.png"
PLATFORM="MSDOS"
ORIGINALLAUNCHER="GODS.EXE"
ORIGINALCONFIG="GODS.INI"
NEEDED_COMMANDS=( unzip wget md5sum ffmpeg dosbox )

if [[ "$1" == "" ]] || [[ "$2" == "" ]]; then
    echo "Usage: $0 [path/to/${CLIENTFILE}] [path/to/install/${TITLE}] [--force]"
    exit 1
fi

function command_exists () {
    # $1: name of needed command to check
    local INFO
    INFO=$( which $1 2>&1 )
    if [[ "${INFO}" == "which: no $1"* ]]; then
        echo ERROR: $1 is not installed on the system.
        exit 2
    fi
}

for command in ${NEEDED_COMMANDS[*]}; do
    command_exists $command
done

if [ ! -f "$1" ]; then
    echo Error: could not find client file \"$1\". You can try --download in its place.
    exit 4
else
    ZIPFILE="$1"
fi

if [[ -e "$2" ]] && [[ ! "$3" == "--force" ]]; then
    echo Error: installation directory \"$2\" already exist. Aborting. You can use --force to proceed.
    exit 6
fi

echo Installing ${TITLE} for Linux

if [ ! -d "$2" ]; then
    echo Creating install directory \"$2\"
    mkdir -p "$2"
fi

echo -n Extracting ${PLATFORM} version of ${TITLE}...
unzip -q -n -X "${ZIPFILE}" -d "$2"
echo Done.

EXEFILE=$(find "$2" -name "${ORIGINALLAUNCHER}" -print)
if [ ! -f "${EXEFILE}" ]; then
    echo "ERROR: Could not find ${ORIGINALLAUNCHER} file. Is it realy a ${TITLE} zip file?"
    exit 7
else
    RELATIVEPATH=${EXEFILE#$2/}
    RELATIVEPATH=${RELATIVEPATH%/$ORIGINALLAUNCHER}
    echo MSDOS Folder to be mounted is \"${RELATIVEPATH}\"
fi

INIFILE=$(find "$2" -name "${ORIGINALCONFIG}" -print)
if [ ! -f "${INIFILE}" ]; then
    echo "ERROR: Could not find ${ORIGINALCONFIG} file. Is it realy a ${TITLE} zip file?"
    exit 8
else
    if ! grep --quiet "Adlib" "${INIFILE}" ; then
        echo -n Patching configuration file "${INIFILE}" to use Adlib audio...
        sed -i 's/Beeper/Adlib/' "${INIFILE}"
        echo Done.
    fi
fi

if [ ! -f "$2/${LINUXCONFIG}" ]; then
    echo -n Creating configuration file "$2/${LINUXCONFIG}"...
    sed "s#~RELATIVEPATH~#${RELATIVEPATH}#" "${0%/*}/${LINUXCONFIG}" > "$2/${LINUXCONFIG}"
    echo Done.
else
    if ! grep --quiet "midiconfig=128:0" "$2/${LINUXEXECUTABLEFOLDER}/${LINUXCONFIG}" ; then
        echo -n Patching configuration file "$2/${LINUXEXECUTABLEFOLDER}/${LINUXCONFIG}"...
        sed -i 's/.*mididevice=.*/&\nmidiconfig=128:0/' "$2/${LINUXEXECUTABLEFOLDER}/${LINUXCONFIG}"
        echo Done.
    fi
fi

echo -n Creating Launcher.sh...
echo "#!/bin/bash
GAMEROOT=\"\${0%/*}\"
cd \"\${GAMEROOT}/${LINUXEXECUTABLEFOLDER}\"

\"${LINUXEXECUTABLE}\" \"\$@\"" > "$2/${LINUXLAUNCHER}"
echo Done.

echo -n Making files executable...
chmod a+x "$2/${LINUXLAUNCHER}" 
echo Done.

echo -n Creating Desktop icon...

if [ ! -f "$2/${LINUXICON}" ]; then
   echo Downloading the Linux icon for ${TITLE}
   wget --no-verbose --no-clobber --progress=bar -O "$2/TEMP_${LINUXICON}" "${ICONURL}"
   retVal=$?
   if [ $retVal -ne 0 ]; then
       echo "ERROR: Trying to download ${ICONURL}".
   else
       ffmpeg -loglevel panic -hide_banner -i "$2/TEMP_${LINUXICON}" -vf scale=-1:256 "$2/${LINUXICON}"
       rm "$2/TEMP_${LINUXICON}"
   fi
fi

if [ -f ~/Desktop/"${TITLE}".desktop ]; then
   echo Skipping: There is already a file called \"~/Desktop/"${TITLE}".desktop\"
   exit 9
fi

GAMEFULLPATH=$( readlink -f "$2" )

echo "[Desktop Entry]
Encoding=UTF-8
Value=1.0
Type=Application
Name=${TITLE}
GenericName=${TITLE}
Comment=${TITLE}
Icon=${GAMEFULLPATH}/${LINUXICON}
Exec=\"${GAMEFULLPATH}/${LINUXLAUNCHER}\"
Categories=Game;
Path=\"${GAMEFULLPATH}\"" > ~/Desktop/"${TITLE}".desktop
echo Done.

