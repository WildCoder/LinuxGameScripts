#!/bin/bash

# This script, install the Linux Client of Unreal Tournament 4, and creates a desktop launch icon.
#
# Thanks to Epic Games for the client: https://www.epicgames.com/unrealtournament/blog/release-notes-june-28

TITLE="Unreal Tournament 4"
CLIENTCL=3525360
CLIENTFILE="UnrealTournament-Client-XAN-${CLIENTCL}-Linux.zip"
CLIENTURL="https://s3.amazonaws.com/unrealtournament/ShippedBuilds/%2B%2BUT%2BRelease-Next-CL-${CLIENTCL}/${CLIENTFILE}"
CLIENTMD5="5ddec99f95fb6e2ee4beffccb1aa24c0"
LINUXEXECUTABLE="UE4-Linux-Shipping"
LINUXEXECUTABLEFOLDER="LinuxNoEditor/Engine/Binaries/Linux"
LINUXPARAMETERS="UnrealTournament"
LINUXLAUNCHER="Launcher.sh"
LINUXICON="${TITLE}.png"
ICONURL="https://www.epicgames.com/unrealtournament/forums/filedata/fetch?id=334140"

NEEDED_COMMANDS=( unzip wget md5sum ffmpeg )

if [[ "$1" == "" ]] || [[ "$2" == "" ]]; then
    echo "Usage: $0 [path/to/${CLIENTFILE} or --download] [path/to/install/${TITLE}] [--force]"
    exit 1
fi

function command_exists () {
    # $1: name of needed command to check
    local INFO
    INFO=$( which $1 2>&1 )
    if [[ "${INFO}" == "which: no $1"* ]]; then
        echo ERROR: $1 is not installed on the system.
        exit 2
    fi
}

for command in ${NEEDED_COMMANDS[*]}; do
    command_exists $command
done

if [ ! -f "$1" ]; then
    if [ "$1" == "--download" ]; then
        echo Downloading the Linux binaries for ${TITLE}
        wget --no-verbose --no-clobber --progress=bar -O "${HOME}/Downloads/${CLIENTFILE}" "${CLIENTURL}"
        retVal=$?
        if [ $retVal -ne 0 ]; then
            echo "ERROR: Trying to download ${CLIENTURL}"
            exit 3
        else
            ZIPFILE="${HOME}/Downloads/${CLIENTFILE}"
        fi
    else
        echo Error: could not find client file \"$1\". You can try --download in its place.
        exit 4
    fi
else
    ZIPFILE="$1"
fi

if [[ ! "${ZIPFILE}" == *"${CLIENTFILE}" ]]; then
    echo Error: \"$1\" does not appear to be the correct version of ${TITLE}.
    exit 5
fi

if [[ -e "$2" ]] && [[ ! "$3" == "--force" ]]; then
    echo Error: installation directory \"$2\" already exist. Aborting. You can use --force to proceed.
    exit 6
fi

echo -n Checking ${CLIENTFILE} integrity...
CHECK=$( md5sum "${ZIPFILE}" )
if [[ ! "${CHECK}" == "${CLIENTMD5}"* ]]; then
    echo ERROR: "${ZIPFILE}" appears to be corrupted. Aborting.
    exit 7
else
    echo Passed.
fi

echo Installing ${TITLE} for Linux

if [ ! -d "$2" ]; then
    echo Creating install directory \"$2\"
    mkdir -p "$2"
fi

echo -n Extracting Linux Client for ${TITLE}...
unzip -q -n -X "${ZIPFILE}" -d "$2"
echo Done.

echo -n Creating Launcher.sh...
echo "#!/bin/bash
GAMEROOT=\"\${0%/*}\"
cd \"\${GAMEROOT}/${LINUXEXECUTABLEFOLDER}\"

\"./${LINUXEXECUTABLE}\" ${LINUXPARAMETERS} \"\$@\"" > "$2/${LINUXLAUNCHER}"
echo Done.

echo -n Making files executable...
chmod a+x "$2/${LINUXLAUNCHER}" 
chmod a+x "$2/${LINUXEXECUTABLEFOLDER}/${LINUXEXECUTABLE}" 
echo Done.

echo -n Creating Desktop icon...

if [ ! -f "$2/${LINUXICON}" ]; then
   echo Downloading the Linux icon for ${TITLE}
   wget --no-verbose --no-clobber --progress=bar -O "$2/TEMP_${LINUXICON}" "${ICONURL}"
   retVal=$?
   if [ $retVal -ne 0 ]; then
       echo "ERROR: Trying to download ${ICONURL}".
   else
       ffmpeg -loglevel panic -hide_banner -i "$2/TEMP_${LINUXICON}" -vf scale=256:256 "$2/${LINUXICON}"
       rm "$2/TEMP_${LINUXICON}"
   fi
fi

if [ -f ~/Desktop/"${TITLE}".desktop ]; then
   echo Skipping: There is already a file called \"~/Desktop/"${TITLE}".desktop\"
   exit 8
fi

GAMEFULLPATH=$( readlink -f "$2" )

echo "[Desktop Entry]
Encoding=UTF-8
Value=1.0
Type=Application
Name=${TITLE}
GenericName=${TITLE}
Comment=${TITLE}
Icon=${GAMEFULLPATH}/${LINUXICON}
Exec=\"${GAMEFULLPATH}/${LINUXLAUNCHER}\"
Categories=Game;
Path=\"${GAMEFULLPATH}/${LINUXEXECUTABLEFOLDER}\"" > ~/Desktop/"${TITLE}".desktop
echo Done.

