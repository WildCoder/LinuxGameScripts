#!/bin/bash

# This script, install the windows version of Unreal Gold (GOG Edition), downloads and patches the Linux binaries, 
# creates a launcher and a desktop launch icon.
# The Launcher make sure that the audio parameters are always correct in the ini file before running the game.
#
# Thanks to the Lutris project for helping figuring out what the ini patches needed to be and hosting some of the file.
# https://lutris.net/games/unreal-gold/

TITLE="Unreal Gold"
GOGURL="https://www.gog.com/game/unreal_gold"
SETUP="setup_unreal_gold_"
PATCHFILE="old-unreal-227i.tar.bz2"
PATCHURL="https://lutris.net/files/games/unreal-gold/${PATCHFILE}"
PATCHMD5="fa881f95b868f202e104e5d5677adf42"
LINUXCONFIG="UnrealLinux.ini"
LINUXEXECUTABLE="UnrealXLinux.bin"
LINUXEXECUTABLEFOLDER="app/System"
LINUXLAUNCHER="Launcher.sh"
LINUXICON="${TITLE}.png"
ICONFILE="Unreal.ico"
ICONURL="https://www.gog.com/upload/forum/2009/04/2b236c0a41a9ae220c4d348475817c00b2d309aa.png"

NEEDED_COMMANDS=( innoextract wget md5sum ffmpeg )

if [[ "$1" == "" ]] || [[ "$2" == "" ]]; then
    echo Usage: $0 [path/to/setup_unreal_gold_*.exe] [path/to/install/Unreal\ Gold] [--force]
    exit 1
fi

if [ ! -f "$1" ]; then
   echo Error: could not find setup file \"$1\". Look there: ${GOGURL}
   exit 2
fi

if [[ ! "$1" == *"${SETUP}"*.exe ]]; then
   echo Error: \"$1\" does not appear to be the GOG Edition of ${TITLE}.
   exit 3
fi

if [[ -e "$2" ]] && [[ ! "$3" == "--force" ]]; then
   echo Error: installation directory \"$2\" already exist. Aborting. You can use --force to proceed.
   exit 4
fi

function command_exists () {
    # $1: name of needed command to check
    local INFO
    INFO=$( which $1 2>&1 )
    if [[ "${INFO}" == "which: no $1"* ]]; then
        echo ERROR: $1 is not installed on the system.
        exit 5
    fi
}

for command in ${NEEDED_COMMANDS[*]}
do
    command_exists $command
done

echo Installing ${TITLE} \(GOG Edition\) for Linux in \"$2\"

if [ ! -d "$2" ]; then
    echo Creating install directory \"$2\"
    mkdir -p "$2"
fi

echo Extracting ${TITLE} for Windows to \"$2\"
innoextract --silent --progress --extract "$1" --output-dir "$2"

if [ ! -f "$2/${PATCHFILE}" ]; then
   echo Downloading the Linux binaries for ${TITLE}
   wget --no-verbose --no-clobber --progress=bar -O "$2/${PATCHFILE}" "${PATCHURL}"
   retVal=$?
   if [ $retVal -ne 0 ]; then
       echo "ERROR: Trying to download ${PATCHURL}"
       exit 6
   fi
fi

echo -n Checking ${PATCHFILE} integrity...
CHECK=$( md5sum "$2/${PATCHFILE}" )
if [[ ! "${CHECK}" == "${PATCHMD5}"* ]]; then
    echo ERROR: "$2/${PATCHFILE}" appears to be corrupted. Aborting.
    exit 7
else
    echo Passed.
fi

echo -n Extracting Linux patch for ${TITLE}...
tar -xf "$2/${PATCHFILE}" --strip-components=1 --directory "$2/app/"
echo Done.

echo -n Creating Launcher.sh...
echo "#!/bin/bash
GAMEROOT=\"\${0%/*}\"
cd \"\${GAMEROOT}/${LINUXEXECUTABLEFOLDER}\"

# Make sure that the first occurence of FMODDevices is set to 27: FMODDevices=27
if ! grep --quiet \"FMODDevices=27\" \"${LINUXCONFIG}\" ; then
    sed -i \"0,/^FMODDevices=.*/s//FMODDevices=27/\" \"${LINUXCONFIG}\"
fi
# Make sure that the first occurence of ALDevices is set to ALSASoftware
if ! grep --quiet \"ALDevices=ALSASoftware\" \"${LINUXCONFIG}\" ; then
    sed -i \"0,/^ALDevices=.*/s//ALDevices=ALSASoftware/\" \"${LINUXCONFIG}\"
fi

MESA_EXTENSION_MAX_YEAR=1999 LD_LIBRARY_PATH=\"../lib\":\".\" ./UnrealLinux.bin \"\$@\"" > "$2/${LINUXLAUNCHER}"
echo Done.

echo -n Making files executable...
chmod a+x "$2/${LINUXLAUNCHER}" 
chmod a+x "$2/${LINUXEXECUTABLEFOLDER}/${LINUXEXECUTABLE}" 
echo Done.

echo -n Creating Desktop icon...

if [ ! -f "$2/${LINUXICON}" ]; then
   echo Downloading the Linux icon for ${TITLE}
   wget --no-verbose --no-clobber --progress=bar -O "$2/TEMP_${LINUXICON}" "${ICONURL}"
   retVal=$?
   if [ $retVal -ne 0 ]; then
       echo "ERROR: Trying to download ${ICONURL}". Converting the windows icon instead.
       # Converting the windows .ico to png doesn't lead to great definition
       ffmpeg -loglevel panic -hide_banner -i "$2/${LINUXEXECUTABLEFOLDER}/${ICONFILE}" -vf scale=256:-1 "$2/${LINUXICON}"
   else
       ffmpeg -loglevel panic -hide_banner -i "$2/TEMP_${LINUXICON}" -vf scale=256:256 "$2/${LINUXICON}"
       rm "$2/TEMP_${LINUXICON}"
   fi
fi

if [ -f ~/Desktop/"${TITLE}".desktop ]; then
   echo Skipping: There is already a file called \"~/Desktop/"${TITLE}".desktop\"
   exit 7
fi

GAMEFULLPATH=$( readlink -f "$2" )

echo "[Desktop Entry]
Encoding=UTF-8
Value=1.0
Type=Application
Name=${TITLE}
GenericName=${TITLE}
Comment=${TITLE}
Icon=${GAMEFULLPATH}/${LINUXICON}
Exec=\"${GAMEFULLPATH}/${LINUXLAUNCHER}\"
Categories=Game;
Path=\"${GAMEFULLPATH}/${LINUXEXECUTABLEFOLDER}\"" > ~/Desktop/"${TITLE}".desktop
echo Done.
