#!/bin/bash

# This script, install the Windows version of The Lost Vikings, and creates a desktop launch icon.
#
# Thanks to Blizards Entertainment for making the game freely available: https://us.battle.net/account/management/download/

TITLE="The Lost Vikings"
CLIENTFILE="The_Lost_Vikings.zip"
CLIENTURL="http://dist.blizzard.com/downloads/classics/9f8ba2a406416a70cfe1bc97ec5346ba/${CLIENTFILE}"
CLIENTMD5="5f61051a66947524e19e00f09f6431a1"
CLIENTDATA="DATA.DAT"
LINUXCONFIG="dosbox.conf"
LINUXEXECUTABLE="dosbox"
LINUXEXECUTABLEFOLDER="The Lost Vikings"
LINUXLAUNCHER="Launcher.sh"
LINUXICON="${TITLE}.png"
ICONURL="https://i.pinimg.com/originals/b5/ff/ab/b5ffab739535220d59f6008d55cdb8e4.png"

NEEDED_COMMANDS=( unzip wget md5sum ffmpeg dosbox )

if [[ "$1" == "" ]] || [[ "$2" == "" ]]; then
    echo "Usage: $0 [path/to/${CLIENTFILE} or --download] [path/to/install/${TITLE}] [--force]"
    exit 1
fi

function command_exists () {
    # $1: name of needed command to check
    local INFO
    INFO=$( which $1 2>&1 )
    if [[ "${INFO}" == "which: no $1"* ]]; then
        echo ERROR: $1 is not installed on the system.
        exit 2
    fi
}

for command in ${NEEDED_COMMANDS[*]}; do
    command_exists $command
done

if [ ! -f "$1" ]; then
    if [ "$1" == "--download" ]; then
        echo Downloading the Linux binaries for ${TITLE}
        wget --no-verbose --no-clobber --progress=bar -O "${HOME}/Downloads/${CLIENTFILE}" "${CLIENTURL}"
        retVal=$?
        if [ $retVal -ne 0 ]; then
            echo "ERROR: Trying to download ${CLIENTURL}"
            exit 3
        else
            ZIPFILE="${HOME}/Downloads/${CLIENTFILE}"
        fi
    else
        echo Error: could not find client file \"$1\". You can try --download in its place.
        exit 4
    fi
else
    ZIPFILE="$1"
fi

if [[ ! "${ZIPFILE}" == *"${CLIENTFILE}" ]]; then
    echo Error: \"$1\" does not appear to be the correct version of ${TITLE}.
    exit 5
fi

if [[ -e "$2" ]] && [[ ! "$3" == "--force" ]]; then
    echo Error: installation directory \"$2\" already exist. Aborting. You can use --force to proceed.
    exit 6
fi

echo -n Checking ${CLIENTFILE} integrity...
CHECK=$( md5sum "${ZIPFILE}" )
if [[ ! "${CHECK}" == "${CLIENTMD5}"* ]]; then
    echo ERROR: "${ZIPFILE}" appears to be corrupted. Aborting.
    exit 7
else
    echo Passed.
fi

echo Installing ${TITLE} for Linux

if [ ! -d "$2" ]; then
    echo Creating install directory \"$2\"
    mkdir -p "$2"
fi

echo -n Extracting Windows version of ${TITLE}...
unzip -q -n -X "${ZIPFILE}" -d "$2"
echo Done.

if ! grep --quiet "midiconfig=128:0" "$2/${LINUXEXECUTABLEFOLDER}/${LINUXCONFIG}" ; then
    echo -n Patching configuration file "$2/${LINUXEXECUTABLEFOLDER}/${LINUXCONFIG}"...
    sed -i 's/.*mididevice=.*/&\nmidiconfig=128:0/' "$2/${LINUXEXECUTABLEFOLDER}/${LINUXCONFIG}"
    echo Done.
fi

# Note: Blizzard defaulted to Roland Audio, and didn't provide a SETUP.EXE to change it.
# We change the number in the data file directly at offset 2154.
# The data looks like: 01 00 00 00 00 00 08 00 04 00 30 03 and we replace the 08 by 05 and the PORT to 220
# Dangerous but we did check the md5sum of the zip file for a reason.
echo -n Patching setup data to use Sound Blaster Pro...
echo -ne \\x05\\x00\\x02\\x00\\x20\\x02 | dd conv=notrunc bs=1 count=6 seek=2154 of="$2/${LINUXEXECUTABLEFOLDER}/VIKINGS/${CLIENTDATA}"
echo Done.

echo -n Creating Launcher.sh...
echo "#!/bin/bash
GAMEROOT=\"\${0%/*}\"
cd \"\${GAMEROOT}/${LINUXEXECUTABLEFOLDER}\"

\"${LINUXEXECUTABLE}\" \"\$@\"" > "$2/${LINUXLAUNCHER}"
echo Done.

echo -n Making files executable...
chmod a+x "$2/${LINUXLAUNCHER}" 
echo Done.

echo -n Creating Desktop icon...

if [ ! -f "$2/${LINUXICON}" ]; then
   echo Downloading the Linux icon for ${TITLE}
   wget --no-verbose --no-clobber --progress=bar -O "$2/TEMP_${LINUXICON}" "${ICONURL}"
   retVal=$?
   if [ $retVal -ne 0 ]; then
       echo "ERROR: Trying to download ${ICONURL}".
   else
       ffmpeg -loglevel panic -hide_banner -i "$2/TEMP_${LINUXICON}" -vf scale=256:256 "$2/${LINUXICON}"
       rm "$2/TEMP_${LINUXICON}"
   fi
fi

if [ -f ~/Desktop/"${TITLE}".desktop ]; then
   echo Skipping: There is already a file called \"~/Desktop/"${TITLE}".desktop\"
   exit 8
fi

GAMEFULLPATH=$( readlink -f "$2" )

echo "[Desktop Entry]
Encoding=UTF-8
Value=1.0
Type=Application
Name=${TITLE}
GenericName=${TITLE}
Comment=${TITLE}
Icon=${GAMEFULLPATH}/${LINUXICON}
Exec=\"${GAMEFULLPATH}/${LINUXLAUNCHER}\"
Categories=Game;
Path=\"${GAMEFULLPATH}\"" > ~/Desktop/"${TITLE}".desktop
echo Done.

