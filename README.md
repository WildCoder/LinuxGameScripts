LinuxGameScripts
================
A small collection of (mostly bash) scripts to install and/or make video games run under Linux

Game Title | Platform | Year | Store / Download
---------- | -------- | ---- | ----------------
Gods | DOS | 1991 | N/A
The Lost Vikings | DOS + Windows | 1992 | https://us.battle.net/account/management/download/
The Lost Vikings 2 | DOS | 1997 | N/A
Unreal Gold | Windows + Linux Patch | 1999 | https://www.gog.com/game/unreal_gold
Unreal Tournament 4 | Linux | | https://www.epicgames.com/unrealtournament/blog/release-notes-june-28

If you like using GUI and having support for a lot of games, you may want to check out the excellent [Lutris](https://lutris.net/) project.